package com.datacollector.app.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.datacollector.app.R
import com.datacollector.app.fragments.main.MainNavHostFragment
import com.datacollector.app.local.AppDao
import com.datacollector.app.utils.FROM_UPDATE
import com.datacollector.app.utils.SharedPreferencesStorage
import kotlinx.android.synthetic.main.fragment_bottom_nav.*
import java.sql.Connection
import javax.inject.Inject

class BottomNavFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bottom_nav, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onRestoreInstanceState()
        if (this.arguments != null) {
            var int = this.arguments?.getInt(FROM_UPDATE)
            if (int == 1) navController().navigate(R.id.introFragment)
        }
    }

    private fun onRestoreInstanceState() {
        val host = childFragmentManager
            .findFragmentById(R.id.bottom_nav_fragment_container)
        host?.let {
           nav_view.setupWithNavController(navController())
        } ?: createNavHostFragment()
    }

    private fun createNavHostFragment() {
        val navHost = MainNavHostFragment.create(R.navigation.bottom_navigation)
        childFragmentManager.beginTransaction()
            .replace(R.id.bottom_nav_fragment_container, navHost, "BottomNavHost")
            .setPrimaryNavigationFragment(navHost)
            .commitNow()
        nav_view.setupWithNavController(navController())
    }

    fun navController(): NavController {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.bottom_nav_fragment_container) as NavHostFragment
        return navHostFragment.navController
    }
}