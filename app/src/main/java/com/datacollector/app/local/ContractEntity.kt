package com.datacollector.app.local

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "contract_entity")
data class ContractEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "contract_id")
    val contractId: Int,
    @ColumnInfo(name = "contract_title")
    val contractTitle: String
): Parcelable