package com.datacollector.app.utils

const val FROM_UPDATE = "FROM_UPDATE"

fun reportingYears(): ArrayList<String> {
    return arrayListOf(
        "2021",
        "2020",
        "2019"
    )
}

fun reportingMonths(): ArrayList<String> {
    return arrayListOf(
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nob",
        "Dec"
    )
}

fun yesNo(): ArrayList<String> {
    return arrayListOf(
        "Yes",
        "No"
    )
}


