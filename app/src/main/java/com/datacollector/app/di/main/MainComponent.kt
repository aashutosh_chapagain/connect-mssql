package  com.datacollector.app.di.main

import com.datacollector.app.ui.main.MainActivity
import dagger.Subcomponent

@MainScope
@Subcomponent(modules = [
    MainModule::class,
    MainFragmentModule::class])
interface MainComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create() : MainComponent
    }

    fun inject(mainActivity: MainActivity)
}