package  com.datacollector.app.di.auth

import com.datacollector.app.ui.auth.AuthActivity
import dagger.Subcomponent

@AuthScope
@Subcomponent(modules = [
    AuthModule::class,
    AuthFragmentModule::class])
interface AuthComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create() : AuthComponent
    }

    fun inject(mainActivity: AuthActivity)
}