package  com.datacollector.app.di.auth

import androidx.fragment.app.FragmentFactory
import com.datacollector.app.fragments.auth.AuthFragmentFactory
import com.datacollector.app.utils.SharedPreferencesStorage
import com.datacollector.app.fragments.main.MainFragmentFactory
import com.datacollector.app.local.AppDao
import dagger.Module
import dagger.Provides
import java.sql.Connection

@Module
object AuthFragmentModule {

    @JvmStatic
    @AuthScope
    @Provides
    fun provideAuthFragmentFactory(
        prefs: SharedPreferencesStorage,
        dao: AppDao
    ) : FragmentFactory {
        return AuthFragmentFactory(prefs, dao)
    }
}