package com.datacollector.app.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentFactory
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.datacollector.app.BaseActivity
import com.datacollector.app.BaseApplication
import com.datacollector.app.R
import com.datacollector.app.fragments.main.MainNavHostFragment
import com.datacollector.app.utils.InternetCheck
import com.datacollector.app.utils.RemoteDatabaseConnection
import com.datacollector.app.utils.SharedPreferencesStorage
import java.sql.Connection
import javax.inject.Inject

class MainActivity : BaseActivity() {

    override fun inject() {
        (application as BaseApplication).mainComponent().inject(this)
    }
    @Inject
    lateinit var fragmentFactory: FragmentFactory
    @Inject
    lateinit var prefs: SharedPreferencesStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = SharedPreferencesStorage(
            application
        )
        onRestoreInstanceState()
    }

    private fun onRestoreInstanceState() {
        val host = supportFragmentManager
            .findFragmentById(R.id.main_fragment_container)
        host?.let {
        } ?: createNavHostFragment()
    }

    private fun createNavHostFragment() {
        val navHost = MainNavHostFragment.create(R.navigation.main_navigation)
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_fragment_container, navHost, "MainNavHost")
            .setPrimaryNavigationFragment(navHost)
            .commit()
    }

    fun navController(): NavController {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_fragment_container) as NavHostFragment
        return navHostFragment.navController
    }

    override fun onDestroy() {
        (application as BaseApplication).releaseMainComponent()
        super.onDestroy()
    }

    private fun checkInternetConnection() {
        InternetCheck(object : InternetCheck.Consumer {
            override fun accept(internet: Boolean?) {
                if (!internet!!) {
                    Toast.makeText(this@MainActivity, "No internet connection", Toast.LENGTH_SHORT).show()
                } else {
                }
            }
        })
    }

   /* override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkStateReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        if (networkStateReceiver != null)
            unregisterReceiver(networkStateReceiver)
    }


    private val networkStateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val connectivityManager =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val wifi: NetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            val mobile: NetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if (wifi.isConnected.not() && mobile.isConnected.not()) {
                Toast.makeText(this@MainActivity, "No internet connection", Toast.LENGTH_SHORT).show()
            } else {
                checkInternetConnection()
            }
        }
    }*/

}