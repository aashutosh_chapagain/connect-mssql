package com.datacollector.app.local

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "tracking_entity")
data class TrackingEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "entry_id")
    val entryId: Int,

    @ColumnInfo(name = "profile_id")
    val profileId: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "head_of_the_family")
    val headOfTheFamily: String,

    @ColumnInfo(name = "district")
    val district: String,

    @ColumnInfo(name = "ward_no")
    val wardNo: String,

    @ColumnInfo(name = "tole")
    val tole: String,

    @ColumnInfo(name = "mobile_number")
    val mobileNumber: String,

    @ColumnInfo(name = "reporting_year")
    val reportingYear: String,

    @ColumnInfo(name = "reporting_month")
    val reportingMonth: String,

    @ColumnInfo(name = "income_exc_agriculture")
    val incomeExcAgriculture: String,

    @ColumnInfo(name = "total_income_agriculture")
    val totalIncomeAgriculture: String,

    @ColumnInfo(name = "qty_produced")
    val qtyProduced: String,

    @ColumnInfo(name = "production_cost")
    val productionCost: String,

    @ColumnInfo(name = "qty_sold")
    val qtySold: String,

    @ColumnInfo(name = "total_earned_from_sell")
    val totalEarnedFromSell: String,

    @ColumnInfo(name = "total_saving")
    val totalSaving: String,

    @ColumnInfo(name = "riverbank_area")
    val riverbankArea: String,

    @ColumnInfo(name = "total_cultivation_area")
    val totalCultivationArea: String,

    @ColumnInfo(name = "irrigation_area")
    val irrigationArea: String,

    @ColumnInfo(name = "land_meas_unit")
    val landMeasUnit: String,

    @ColumnInfo(name = "days_worked")
    val daysWorked: String,

    @ColumnInfo(name = "income_per_day")
    val incomePerDay: String,

    @ColumnInfo(name = "income_made_from_emp")
    val incomeMadeFromEmp: String,

    @ColumnInfo(name = "received_skills")
    val receivedSkills: String,

    @ColumnInfo(name = "engagement")
    val engagement: String,

    @ColumnInfo(name = "received_services")
    val receivedServices: String,

    @ColumnInfo(name = "remarks")
    val remarks: String

) : Parcelable