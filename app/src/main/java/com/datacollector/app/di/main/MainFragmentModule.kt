package  com.datacollector.app.di.main

import androidx.fragment.app.FragmentFactory
import com.datacollector.app.utils.SharedPreferencesStorage
import com.datacollector.app.fragments.main.MainFragmentFactory
import com.datacollector.app.local.AppDao
import dagger.Module
import dagger.Provides
import java.sql.Connection

@Module
object MainFragmentModule {

    @JvmStatic
    @MainScope
    @Provides
    fun provideMainFragmentFactory(
        prefs: SharedPreferencesStorage,
        connection: Connection?,
        dao: AppDao
    ) : FragmentFactory {
        return MainFragmentFactory(prefs, connection, dao)
    }
}