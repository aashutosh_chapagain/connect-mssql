package com.datacollector.app.ui.main

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.datacollector.app.R
import com.datacollector.app.databinding.FragmentMonthlyProgressBinding
import com.datacollector.app.local.AppDao
import com.datacollector.app.local.TrackingEntity
import com.datacollector.app.ui.SplashActivity
import com.datacollector.app.utils.FROM_UPDATE
import com.datacollector.app.utils.SharedPreferencesStorage
import com.datacollector.app.utils.SpinnerAdapter
import com.datacollector.app.utils.yesNo
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_bottom_nav.view.*
import java.sql.Connection
import javax.inject.Inject

class MonthlyProgressFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var binding: FragmentMonthlyProgressBinding
    lateinit var args: MonthlyProgressFragmentArgs

    private var yesNoArray: List<String> = ArrayList()

    private var receivedSkill = ""
    private var engagement = ""
    private var receivedServices = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMonthlyProgressBinding.inflate(layoutInflater)
        return binding.root
    }

    private fun observeTrackingEntity() {
        dao.getLocalTrackingList().removeObservers(viewLifecycleOwner)
        dao.getLocalTrackingList().observe(viewLifecycleOwner, Observer {
            it.forEach { entity ->
                if (entity.profileId == args.progresstrackingmodel.profileInt) {
                    initViews(entity)
                } else {
                    val a = 1
                }
            }
        })
    }

    private fun initViews(it: TrackingEntity) {
        binding.etIncomeExcAgriculture.setText(it.incomeExcAgriculture)
        binding.etTotalIncomeAgriculture.setText(it.totalIncomeAgriculture)
        binding.etQtyProduced.setText(it.qtyProduced)
        binding.etProductionCost.setText(it.productionCost)
        binding.etQtySold.setText(it.qtySold)
        binding.etTotalEarnedFromSell.setText(it.totalEarnedFromSell)
        binding.etTotalSaving.setText(it.totalSaving)
        binding.etTotalCultivationArea.setText(it.totalCultivationArea)
        binding.etRiverbankArea.setText(it.riverbankArea)
        binding.etIrrigationArea.setText(it.irrigationArea)
        binding.etDaysWorked.setText(it.daysWorked)
        binding.etIncomePerDay.setText(it.incomePerDay)
        binding.etIncomeMadeFromEmp.setText(it.incomeMadeFromEmp)
        binding.etRemarks.setText(it.remarks)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        arguments?.let {
            args = MonthlyProgressFragmentArgs.fromBundle(it)
        }
        observeTrackingEntity()
        args.progresstrackingmodel
        binding.spinnerReceivedSkills.onItemSelectedListener = this
        binding.spinnerEngagement.onItemSelectedListener = this
        binding.spinnerReceivedServices.onItemSelectedListener = this
        binding.etLandMeasUnit.setText(args.progresstrackingmodel.landUnit)
        initReceivedSkills()
        initEngagement()
        initReceivedServices()

        binding.etTotalEarnedFromSell.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val productionCost = binding.etProductionCost.text?.trim().toString()
                if (!productionCost.isNullOrEmpty()) {
                    if (!p0.toString().isNullOrEmpty()) {
                        if (productionCost.toInt() <= p0.toString().toInt()) {
                            binding.etTotalSaving.setText("${p0.toString().toInt() - productionCost.toInt()}")
                        } else {
                            binding.etTotalSaving.setText("")
                        }
                        if (p0.toString().toInt() > productionCost.toInt() * 1.6) {
                            val alertDialog = MaterialAlertDialogBuilder(requireContext())
                            alertDialog.setTitle("Total amount earned from sell in NPR")
                            alertDialog.setMessage("Total amount earned from sell in NPR seems to be unusual, still you want to continue?")
                            alertDialog.setPositiveButton("Yes") { dialog, _ ->
                                dialog.dismiss()
                            }
                            alertDialog.setNegativeButton("No") { dialog, _ ->
                                binding.etTotalEarnedFromSell.setText("")
                                dialog.dismiss()
                            }
                            alertDialog.show()
                        }
                    } else {
                        binding.etTotalSaving.setText("")
                    }
                } else {
                    binding.etTotalSaving.setText("")
                    Toast.makeText(requireContext(), "Please Enter Production Cost!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun afterTextChanged(p0: Editable?) {}

        })

        binding.etProductionCost.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val totalEarnedFromSell = binding.etTotalEarnedFromSell.text?.trim().toString()
                if (!totalEarnedFromSell.isNullOrEmpty()) {
                    if (!p0.toString().isNullOrEmpty()) {
                        if (p0.toString().toInt() <= totalEarnedFromSell.toInt()) {
                            binding.etTotalSaving.setText("${totalEarnedFromSell.toInt() - p0.toString().toInt()}")
                        } else {
                            binding.etTotalSaving.setText("")
                        }
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) {}

        })


        binding.btnSave.setOnClickListener {
            val incomeExcAgriculture = binding.etIncomeExcAgriculture.text?.trim().toString()
            val totalIncomeAgriculture = binding.etTotalIncomeAgriculture.text?.trim().toString()
            val qtyProduced = binding.etQtyProduced.text?.trim().toString()
            val productionCost = binding.etProductionCost.text?.trim().toString()
            val qtySold = binding.etQtySold.text?.trim().toString()
            val totalEarnerFromSell = binding.etTotalEarnedFromSell.text?.trim().toString()
            val totalSaving = binding.etTotalSaving.text?.trim().toString()
            val riverBankArea = binding.etRiverbankArea.text?.trim().toString()
            val totalCultivationArea = binding.etTotalCultivationArea.text?.trim().toString()
            val irrigationArea = binding.etIrrigationArea.text?.trim().toString()
            val landMeasUnit = binding.etLandMeasUnit.text?.trim().toString()
            val daysWorked = binding.etDaysWorked.text?.trim().toString()
            val incomePerDay = binding.etIncomePerDay.text?.trim().toString()
            val incomeMadeFromEmp = binding.etIncomeMadeFromEmp.text?.trim().toString()
            val remarks = binding.etRemarks.text?.trim().toString()

            if (incomeExcAgriculture.isEmpty()) {
                setErrorMessage(binding.etIncomeExcAgriculture)
            }
            if (totalIncomeAgriculture.isEmpty()) {
                setErrorMessage(binding.etTotalIncomeAgriculture)
            }
            if (qtyProduced.isEmpty()) {
                setErrorMessage(binding.etQtyProduced)
            }
            if (productionCost.isEmpty()) {
                setErrorMessage(binding.etProductionCost)
            }
            if (qtySold.isEmpty()) {
                setErrorMessage(binding.etQtySold)
            }
            if (totalEarnerFromSell.isEmpty()) {
                setErrorMessage(binding.etTotalEarnedFromSell)
            }
            if (totalSaving.isEmpty()) {
                setErrorMessage(binding.etTotalSaving)
            }
            if (riverBankArea.isEmpty()) {
                setErrorMessage(binding.etRiverbankArea)
            }
            if (totalCultivationArea.isEmpty()) {
                setErrorMessage(binding.etTotalCultivationArea)
            }
            if (irrigationArea.isEmpty()) {
                setErrorMessage(binding.etIrrigationArea)
            }
//            if (landMeasUnit.isEmpty()) {
//                setErrorMessage(binding.etLandMeasUnit)
//            }
            if (daysWorked.isEmpty()) {
                setErrorMessage(binding.etDaysWorked)
            }
            if (incomePerDay.isEmpty()) {
                setErrorMessage(binding.etIncomePerDay)
            }
            if (incomeMadeFromEmp.isEmpty()) {
                setErrorMessage(binding.etIncomeMadeFromEmp)
            }
            if (
                incomeExcAgriculture.isNotEmpty() &&
                totalIncomeAgriculture.isNotEmpty() &&
                qtyProduced.isNotEmpty() &&
                productionCost.isNotEmpty() &&
                qtySold.isNotEmpty() &&
                totalEarnerFromSell.isNotEmpty() &&
                totalSaving.isNotEmpty() &&
                riverBankArea.isNotEmpty() &&
                totalCultivationArea.isNotEmpty() &&
                irrigationArea.isNotEmpty() &&
                daysWorked.isNotEmpty() &&
                incomePerDay.isNotEmpty() &&
                incomeMadeFromEmp.isNotEmpty()
            ) {
                if (dao.getLocalTrackingListRaw(args.progresstrackingmodel.profileInt).isEmpty()) {
                    val trackingEntity = TrackingEntity(
                        0,
                        args.progresstrackingmodel.profileInt,
                        args.progresstrackingmodel.name,
                        args.progresstrackingmodel.headOfTheFamily,
                        args.progresstrackingmodel.district,
                        args.progresstrackingmodel.wardNo,
                        args.progresstrackingmodel.tole,
                        args.progresstrackingmodel.mobileNumber,
                        args.progresstrackingmodel.reportingYear,
                        args.progresstrackingmodel.reportingMonth,
                        incomeExcAgriculture,
                        totalIncomeAgriculture,
                        qtyProduced,
                        productionCost,
                        qtySold,
                        totalEarnerFromSell,
                        totalSaving,
                        riverBankArea,
                        totalCultivationArea,
                        irrigationArea,
                        landMeasUnit,
                        daysWorked,
                        incomePerDay,
                        incomeMadeFromEmp,
                        receivedSkill,
                        engagement,
                        receivedServices,
                        remarks
                    )
                    dao.addEntry(trackingEntity)
                    dao.updateProfileByMobile(args.progresstrackingmodel.profileInt, true)
                    val bundle = Bundle()
                    bundle.putInt(FROM_UPDATE, 1)
                    (activity as MainActivity).navController()
                        .navigate(R.id.bottomNavFragment, bundle)
                } else {
                    dao.updateTrackingEntityByMobile(
                        args.progresstrackingmodel.reportingYear,
                        args.progresstrackingmodel.reportingMonth,
                        incomeExcAgriculture,
                        totalIncomeAgriculture,
                        qtyProduced,
                        productionCost,
                        qtySold,
                        totalEarnerFromSell,
                        totalSaving,
                        riverBankArea,
                        totalCultivationArea,
                        irrigationArea,
                        landMeasUnit,
                        daysWorked,
                        incomePerDay,
                        incomeMadeFromEmp,
                        receivedSkill,
                        engagement,
                        receivedServices,
                        remarks,
                        args.progresstrackingmodel.profileInt
                    )
                    dao.updateProfileByMobile(args.progresstrackingmodel.profileInt, true)
                    val bundle = Bundle()
                    bundle.putInt(FROM_UPDATE, 1)
                    (activity as MainActivity).navController()
                        .navigate(R.id.bottomNavFragment, bundle)
                }
            }
        }
    }

    private fun setErrorMessage(editText: AppCompatEditText) {
        editText.apply {
            requestFocus()
            error = "Required Field"
        }
    }

    private fun initReceivedServices() {
        yesNoArray = yesNo()
        binding.spinnerReceivedSkills.apply {
            adapter = SpinnerAdapter(requireContext(), yesNoArray)
        }
    }

    private fun initEngagement() {
        yesNoArray = yesNo()
        binding.spinnerEngagement.apply {
            adapter = SpinnerAdapter(requireContext(), yesNoArray)
        }
    }

    private fun initReceivedSkills() {
        yesNoArray = yesNo()
        binding.spinnerReceivedServices.apply {
            adapter = SpinnerAdapter(requireContext(), yesNoArray)
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        when (parent) {
            binding.spinnerReceivedSkills -> {
                receivedSkill = yesNoArray[position]
            }
            binding.spinnerEngagement -> {
                engagement = yesNoArray[position]
            }
            binding.spinnerReceivedServices -> {
                receivedServices = yesNoArray[position]
            }
        }
    }


}