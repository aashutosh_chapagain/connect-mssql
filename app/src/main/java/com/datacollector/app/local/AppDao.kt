package com.datacollector.app.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AppDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addProfile(profileEntity: ProfileEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addPartner(partnerEntity: PartnerEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addContract(contractEntity: ContractEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addEntry(trackingEntity: TrackingEntity)

    @Query("SELECT * FROM profile_entity")
    fun getLocalProfileList(): List<ProfileEntity>

    @Query("SELECT * FROM profile_entity")
    fun getProfileList(): LiveData<List<ProfileEntity>>

    @Query("SELECT * FROM partner_entity")
    fun getLocalPartnerList(): LiveData<List<PartnerEntity>>

    @Query("SELECT * FROM contract_entity")
    fun getLocalContractList(): LiveData<List<ContractEntity>>

    @Query("SELECT * FROM tracking_entity")
    fun getLocalTrackingList(): LiveData<List<TrackingEntity>>

    @Query("SELECT * FROM tracking_entity WHERE profile_id = :profileInt")
    fun getLocalTrackingListRaw(profileInt: Int): List<TrackingEntity>

    @Query("UPDATE tracking_entity SET reporting_year = :reportingYear, reporting_month = :reportingMonth, income_exc_agriculture = :incomeExcAgriculture,  total_income_agriculture = :totalIncomeAgriculture, qty_produced = :qtyProduced, production_cost = :productionCost, qty_sold = :qtySold, total_earned_from_sell = :totalEarnedFromSell, total_saving = :totalSaving, riverbank_area = :riverbankArea, total_cultivation_area = :totalCultivationArea, irrigation_area = :irrigationArea, land_meas_unit = :landMeasUnit, days_worked = :daysWorked, income_per_day = :incomePerDay, income_made_from_emp = :incomeMadeFromEmp, received_skills = :receivedSkills, engagement = :engagement, received_services = :receivedServices, remarks = :remarks  WHERE profile_id =:profileId")
    fun updateTrackingEntityByMobile(reportingYear: String, reportingMonth: String, incomeExcAgriculture: String, totalIncomeAgriculture: String,
                                     qtyProduced: String, productionCost: String, qtySold: String, totalEarnedFromSell: String,
                                     totalSaving: String, riverbankArea: String, totalCultivationArea: String, irrigationArea: String, landMeasUnit: String,
                                     daysWorked: String, incomePerDay: String, incomeMadeFromEmp: String, receivedSkills: String,
                                     engagement: String,  receivedServices: String, remarks: String, profileId: Int)

    @Query("SELECT * FROM profile_entity WHERE name LIKE '%' || :profileName || '%'")
    fun searchLocalProfileByName(profileName: String): LiveData<List<ProfileEntity>>

    @Query("UPDATE profile_entity SET is_updated = :isUpdated WHERE profile_id =:profileInt")
    fun updateProfileByMobile(profileInt: Int, isUpdated: Boolean)

    @Query("DELETE FROM tracking_entity")
    fun clearTrackingData()

    @Query("DELETE FROM profile_entity")
    fun clearProfileData()

    @Query("DELETE FROM partner_entity")
    fun clearPartnerData()

    @Query("DELETE FROM CONTRACT_ENTITY")
    fun clearContractData()
}