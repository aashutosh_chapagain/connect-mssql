package com.datacollector.app.fragments.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.datacollector.app.di.main.MainScope
import com.datacollector.app.local.AppDao
import com.datacollector.app.ui.main.*
import com.datacollector.app.utils.SharedPreferencesStorage
import java.sql.Connection
import javax.inject.Inject

@MainScope
class MainFragmentFactory
@Inject constructor(
    private val prefs: SharedPreferencesStorage,
    private val connection: Connection?,
    private val dao: AppDao
) : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when (className) {
            DownloadFragment::class.java.name -> {
                DownloadFragment(prefs, connection, dao)
            }
            SyncFragment::class.java.name -> {
                SyncFragment(prefs, dao)
            }
            ProfileFormFragment::class.java.name -> {
                ProfileFormFragment(prefs, dao)
            }
            IntroFragment::class.java.name -> {
                IntroFragment(prefs, dao)
            }
            ProgressTrackingFragment::class.java.name -> {
                ProgressTrackingFragment()
            }
            MonthlyProgressFragment::class.java.name -> {
                MonthlyProgressFragment(prefs, dao)
            }
            BottomNavFragment::class.java.name -> {
                BottomNavFragment(prefs, dao)
            }
            else -> {
                BottomNavFragment(prefs, dao)
            }
        }
}