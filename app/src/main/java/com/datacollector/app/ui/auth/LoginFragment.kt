package com.datacollector.app.ui.auth

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.AsyncTask.execute
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.datacollector.app.R
import com.datacollector.app.di.main.MainScope
import com.datacollector.app.local.AppDao
import com.datacollector.app.ui.main.MainActivity
import com.datacollector.app.utils.RemoteDatabaseConnection
import com.datacollector.app.utils.SharedPreferencesStorage
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_login.*
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import javax.inject.Inject

@MainScope
class LoginFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment() {

    lateinit var progressBar: ProgressBar
    lateinit var mLogin: MaterialButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        progressBar = view.findViewById(R.id.progress_bar)
        mLogin = view.findViewById(R.id.btn_login)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.INTERNET),
            PackageManager.PERMISSION_GRANTED
        )
        mLogin.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            sqlButton()
            //Log.i("Login", connection.toString())
        }
    }

    private fun sqlButton() {
        val connection = RemoteDatabaseConnection().execute().get()
        val asyncLogin = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String {
                if (connection != null) {
                    val statement: Statement?
                    val username = et_username.text?.trim().toString()
                    val password = et_password.text?.trim().toString()
                    if (username.isNotEmpty() && password.isNotEmpty()) {
                        return try {
                            statement = connection!!.prepareCall("{call procAuthenticate(?, ?)}")
                            statement.setString(1, username)
                            statement.setString(2, password)
                            statement.execute()
                            val resultSet: ResultSet = statement.resultSet
                            while (resultSet.next()) {
                                if (resultSet.getString("UserID").toString() != prefs.userID) {
                                    clearDatabase()
                                    prefs.clearPrefs()
                                }
                                prefs.apply {
                                    fullName = resultSet.getString("FullName").toString()
                                    userName = resultSet.getString("UserName").toString()
                                    userID = resultSet.getString("UserID").toString()
                                    partnerID = resultSet.getString("PartnerID").getZeroIfNull()
                                    partnerName = resultSet.getString("PartnerName").getEmptyIfNull()
                                    isLogin = true
                                }
                            }
                            statement.close()
                            if (prefs.userID.isNotEmpty()) SUCCESS else INVALID_CREDENTIALS
                        } catch (e: SQLException) {
                            e.printStackTrace()
                            FAILED
                        }
                    } else {
                        return VALIDATION_ERROR
                    }
                } else {
                    return CONNECTION_ERROR
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                if (result == SUCCESS) {
                    startActivity(Intent(requireContext(), MainActivity::class.java))
                    requireActivity().finish()
                    progressBar.visibility = View.GONE
                } else {
                    Snackbar.make(view!!, result.toString(), Snackbar.LENGTH_SHORT).show()
                    progressBar.visibility = View.GONE
                }
            }
        }
        asyncLogin.execute()
    }

    private fun clearDatabase() {
        dao.clearContractData()
        dao.clearPartnerData()
        dao.clearProfileData()
        dao.clearTrackingData()
    }

    fun String?.getZeroIfNull(): String {
        return this ?: "0"
    }

    fun String?.getEmptyIfNull(): String {
        return this ?: ""
    }

    fun String?.getQuotedNullIfNull(): String {
        return this ?: "null"
    }

}