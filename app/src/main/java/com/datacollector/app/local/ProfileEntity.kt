package com.datacollector.app.local

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "profile_entity")
data class ProfileEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "profile_id")
    val profileID: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "family_head_name")
    val familyHeadName: String,
    @ColumnInfo(name = "mobile_no")
    val mobileNo: String,
    @ColumnInfo(name = "father_name")
    val fatherName: String,
    @ColumnInfo(name = "mother_name")
    val motherName: String,
    @ColumnInfo(name = "district")
    val district: String,
    @ColumnInfo(name = "municipality")
    val municipality: String,
    @ColumnInfo(name = "ward_no")
    val wardNo: String,
    @ColumnInfo(name = "tole")
    val tole: String,
    @ColumnInfo(name = "commodity")
    val commodity: String,
    @ColumnInfo(name = "land_unit")
    val landUnit: String,
    @ColumnInfo(name = "is_updated")
    val isUpdated: Boolean
): Parcelable