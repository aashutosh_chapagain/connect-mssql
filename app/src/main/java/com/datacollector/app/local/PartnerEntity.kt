package com.datacollector.app.local

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "partner_entity")
data class PartnerEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "partner_id")
    val partnerId: Int,
    @ColumnInfo(name = "partner_name")
    val partnerName: String
) : Parcelable