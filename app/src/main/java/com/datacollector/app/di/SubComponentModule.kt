package com.datacollector.app.di

import com.datacollector.app.di.auth.AuthComponent
import com.datacollector.app.di.main.MainComponent
import dagger.Module

@Module(
    subcomponents = [
        AuthComponent::class,
        MainComponent::class
    ]
)
class SubComponentModule