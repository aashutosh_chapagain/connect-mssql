package com.datacollector.app.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.datacollector.app.R
import com.datacollector.app.databinding.FragmentProgressTrackingBinding
import com.datacollector.app.models.ProgressTrackingModel
import com.datacollector.app.utils.SpinnerAdapter
import com.datacollector.app.utils.reportingMonths
import com.datacollector.app.utils.reportingYears

class ProgressTrackingFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private var reportingYears: List<String> = ArrayList()
    private var reportingMonths: List<String> = ArrayList()

    private var reportingYear = ""
    private var reportingMonth = ""

    private lateinit var binding: FragmentProgressTrackingBinding

    lateinit var args: ProgressTrackingFragmentArgs


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProgressTrackingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            args = ProgressTrackingFragmentArgs.fromBundle(it)
        }
        initViews()
        binding.spinnerReportingYear.onItemSelectedListener = this
        binding.spinnerReportingMonth.onItemSelectedListener = this
        initYears()
        initMonths()
        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    private fun initViews() {
        binding.etName.setText(args.profileentity.name)
        binding.etFamilyHead.setText(args.profileentity.familyHeadName)
        binding.etDistrict.setText(args.profileentity.district)
        binding.etWard.setText(args.profileentity.wardNo)
        binding.etTole.setText(args.profileentity.tole)
        binding.etMobile.setText(args.profileentity.mobileNo)
    }

    private fun initYears() {
        reportingYears = reportingYears()
        binding.spinnerReportingYear.apply {
            adapter = SpinnerAdapter(requireContext(), reportingYears)
        }
    }

    private fun initMonths() {
        reportingMonths = reportingMonths()
        binding.spinnerReportingMonth.apply {
            adapter = SpinnerAdapter(requireContext(), reportingMonths)
        }
        binding.btnNext.setOnClickListener {
            val progressTrackingModel = ProgressTrackingModel(
                args.profileentity.profileID,
                binding.etName.text?.trim().toString(),
                binding.etFamilyHead.text?.trim().toString(),
                binding.etDistrict.text?.trim().toString(),
                binding.etWard.text?.trim().toString(),
                binding.etTole.text?.trim().toString(),
                binding.etMobile.text?.trim().toString(),
                reportingYear,
                reportingMonth,
                args.profileentity.landUnit
            )
            (activity as MainActivity).navController().navigate(ProgressTrackingFragmentDirections.actionFormFragmentToMonthlyProgressFragment(progressTrackingModel))
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        when (parent) {
            binding.spinnerReportingYear -> {
                reportingYear = reportingYears[position]
            }
            binding.spinnerReportingMonth -> {
                reportingMonth = reportingMonths[position]
            }
        }
    }


}