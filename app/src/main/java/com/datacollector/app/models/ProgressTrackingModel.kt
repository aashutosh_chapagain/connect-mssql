package com.datacollector.app.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProgressTrackingModel(
    val profileInt: Int,
    val name: String,
    val headOfTheFamily: String,
    val district: String,
    val wardNo: String,
    val tole: String,
    val mobileNumber: String,
    val reportingYear: String,
    val reportingMonth: String,
    val landUnit: String
): Parcelable