package com.datacollector.app.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.datacollector.app.R
import com.datacollector.app.databinding.FragmentSyncBinding
import com.datacollector.app.local.AppDao
import com.datacollector.app.local.TrackingEntity
import com.datacollector.app.ui.SplashActivity
import com.datacollector.app.ui.auth.CONNECTION_ERROR
import com.datacollector.app.ui.auth.FAILED
import com.datacollector.app.ui.auth.SUCCESS
import com.datacollector.app.utils.InternetCheck
import com.datacollector.app.utils.RemoteDatabaseConnection
import com.datacollector.app.utils.SharedPreferencesStorage
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.sql.Connection
import java.sql.Statement
import javax.inject.Inject

class SyncFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment() {

    lateinit var binding: FragmentSyncBinding

    var trackingEntityList = arrayListOf<TrackingEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSyncBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInternetConnection()
        observeTrackingEntity()
        binding.btnSync.setOnClickListener {
            val connection = RemoteDatabaseConnection().execute().get()
            if (trackingEntityList.isNotEmpty()) {
                trackingEntityList.forEach {
                    val uploadAsync = @SuppressLint("StaticFieldLeak")
                    object : AsyncTask<Void, Void, String>() {
                        override fun doInBackground(vararg p0: Void?): String {
                            //val remarks = binding.etRemarks.text?.trim().toString()
                            if (connection != null) {
                                val statement: Statement?
                                var resultDesc: String = ""
                                var resultCode: Int = 99
                                return try {
                                    statement =
                                        connection.prepareCall(
                                            "{call InsUpdProfileProgress ${it.profileId},${prefs.userID.toInt()}," +
                                                    "${it.reportingYear.toInt()},'${it.reportingMonth}',${it.incomeExcAgriculture.toInt()}," +
                                                    "${it.totalIncomeAgriculture.toInt()},${it.qtyProduced.toInt()}," +
                                                    "${it.productionCost.toInt()},${it.qtySold.toInt()},${it.totalEarnedFromSell.toInt()}," +
                                                    "${it.totalSaving.toInt()},${it.riverbankArea.toInt()},${it.totalCultivationArea.toInt()},${it.irrigationArea.toInt()},${it.daysWorked.toInt()},${it.incomePerDay.toInt()}," +
                                                    "${it.incomeMadeFromEmp.toInt()},'${it.receivedSkills}','${it.engagement}','${it.receivedServices}', '${it.remarks}'}"
                                        )
                                    statement.execute()
                                    val resultSet = statement.resultSet
                                    while (resultSet.next()) {
                                        resultDesc = resultSet.getString("ResultDescription")
                                        resultCode = resultSet.getInt("ResultCode")
                                    }
                                    if (resultCode == 1) SUCCESS else FAILED
                                } catch (e: Exception) {
                                    e.localizedMessage
                                }
                            } else {
                                return CONNECTION_ERROR
                            }
                        }

                        override fun onPostExecute(result: String?) {
                            super.onPostExecute(result)
                            if (result == SUCCESS) {
                                val profiles = arrayListOf<Int>()
                                trackingEntityList.forEach { trackingEntity ->
                                    profiles.add(trackingEntity.profileId)
                                }
                                profiles.forEach { profileId ->
                                    dao.updateProfileByMobile(profileId, false)
                                }
                                Toast.makeText(
                                    requireContext(),
                                    "Total ${trackingEntityList.count()} ${if (trackingEntityList.count() > 1) "rows" else "row"} synced successfully",
                                    Toast.LENGTH_SHORT
                                ).show()
                                dao.clearTrackingData()
                                trackingEntityList.clear()
                                cancel(true)
                            } else if (result == CONNECTION_ERROR) {
                                Toast.makeText(
                                    requireContext(),
                                    "Connection Failed",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                cancel(true)
                            }
                        }
                    }
                    uploadAsync.execute()
                }
            } else {
                Toast.makeText(requireContext(), "No Record to sync", Toast.LENGTH_SHORT).show()
            }
        }

        binding.ivLogout.setOnClickListener {
            val alertDialog = MaterialAlertDialogBuilder(requireContext())
            alertDialog.setTitle("Log out")
            alertDialog.setMessage("Are you sure to log out?")
            alertDialog.setPositiveButton("Yes") { dialog, _ ->
                // do logout
                prefs.isLogin = false
                startActivity(Intent(requireContext(), SplashActivity::class.java))
                requireActivity().finish()
                dialog.dismiss()
            }
            alertDialog.setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }
    }

   /* private fun reSync(it: TrackingEntity) {
        val newConnection = RemoteDatabaseConnection().execute().get()
        val reUploadAsync = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String {
                //val remarks = binding.etRemarks.text?.trim().toString()
                if (newConnection != null) {
                    val statement: Statement?
                    var resultDesc: String = ""
                    var resultCode: Int = 99
                    return try {
                        statement =
                            newConnection.prepareCall(
                                "{call InsUpdProfileProgress ${it.profileId},${prefs.userID.toInt()}," +
                                        "${it.reportingYear.toInt()},'${it.reportingMonth}',${it.incomeExcAgriculture.toInt()}," +
                                        "${it.totalIncomeAgriculture.toInt()},${it.qtyProduced.toInt()}," +
                                        "${it.productionCost.toInt()},${it.qtySold.toInt()},${it.totalEarnedFromSell.toInt()}," +
                                        "${it.totalSaving.toInt()},${it.riverbankArea.toInt()},${it.irrigationArea.toInt()},${it.daysWorked.toInt()},${it.incomePerDay.toInt()}," +
                                        "${it.incomeMadeFromEmp.toInt()},'${it.receivedSkills}','${it.engagement}','${it.receivedServices}', '${it.remarks}'}"
                            )
                        statement.execute()
                        val resultSet = statement.resultSet
                        while (resultSet.next()) {
                            resultDesc = resultSet.getString("ResultDescription")
                            resultCode = resultSet.getInt("ResultCode")
                        }
                        if (resultCode == 1) SUCCESS else FAILED
                    } catch (e: Exception) {
                        e.localizedMessage
                    }
                } else {
                    return CONNECTION_ERROR
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                if (result == SUCCESS) {
                    val profiles = arrayListOf<Int>()
                    trackingEntityList.forEach { trackingEntity ->
                        profiles.add(trackingEntity.profileId)
                    }
                    profiles.forEach { profileId ->
                        dao.updateProfileByMobile(profileId, false)
                    }
                    Toast.makeText(
                        requireContext(),
                        "Total ${trackingEntityList.count()} ${if (trackingEntityList.count() > 1) "rows" else "row"} synced successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                    dao.clearTrackingData()
                    trackingEntityList.clear()
                    cancel(true)
                } else if (result == CONNECTION_ERROR) {
                    Toast.makeText(requireContext(), "Connection Failed", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "Please try again", Toast.LENGTH_SHORT).show()
                }
            }
        }
        reUploadAsync.execute()
    }*/

    private fun initInternetConnection() {
        InternetCheck(object : InternetCheck.Consumer {
            override fun accept(internet: Boolean?) {
                if (internet!!) {
                    binding?.tvInternetConnection?.apply {
                        text = "Internet Connected"
                        setTextColor(ContextCompat.getColor(requireContext(), R.color.colorGreen))
                    }
                } else {
                    binding?.tvInternetConnection?.apply {
                        text = "No internet connection. Cannot sync now"
                        setTextColor(ContextCompat.getColor(requireContext(), R.color.colorRed))
                    }
                    binding.btnSync.isEnabled = false
                }
            }
        })
    }

    private fun observeTrackingEntity() {
        dao.getLocalTrackingList().removeObservers(viewLifecycleOwner)
        dao.getLocalTrackingList().observe(viewLifecycleOwner, Observer {
            trackingEntityList.addAll(it)
            //binding.tvToBeSynced.text = "${it.count()} entries to be synced"
        })
    }

}