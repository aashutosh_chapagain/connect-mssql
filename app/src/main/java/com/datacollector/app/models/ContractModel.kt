package com.datacollector.app.models

data class ContractModel(
    val contractId: Int,
    val contractTitle: String
)