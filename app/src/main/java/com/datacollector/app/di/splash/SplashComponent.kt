package com.datacollector.app.di.splash

import com.datacollector.app.ui.SplashActivity
import dagger.Subcomponent

@SplashScope
@Subcomponent
interface SplashComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create() : SplashComponent
    }

    fun inject(splashActivity: SplashActivity)
}