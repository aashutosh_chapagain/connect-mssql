package com.datacollector.app.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.datacollector.app.*
import com.datacollector.app.local.AppDao
import com.datacollector.app.ui.SplashActivity
import com.datacollector.app.utils.SharedPreferencesStorage
import kotlinx.android.synthetic.main.fragment_profile_form.*
import java.sql.Connection
import javax.inject.Inject

class ProfileFormFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_logout.setOnClickListener {
            prefs.clearPrefs()
            startActivity(Intent(requireContext(), SplashActivity::class.java))
            activity?.finish()
        }
    }
}