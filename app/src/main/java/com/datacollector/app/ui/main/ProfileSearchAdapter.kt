package com.datacollector.app.ui.main

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.datacollector.app.R
import com.datacollector.app.local.ProfileEntity
import com.datacollector.app.utils.SharedPreferencesStorage
import com.google.android.material.button.MaterialButton

class ProfileSearchAdapter(
    private val interaction: Interaction? = null,
    private val prefs: SharedPreferencesStorage
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    private val diffCallback = object : DiffUtil.ItemCallback<ProfileEntity>() {

        override fun areItemsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity):
                Boolean = oldItem.profileID == newItem.profileID

        override fun areContentsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity)
                : Boolean = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return ProfileSearchVH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_profile_search,
                parent,
                false
            ),
            interaction, prefs
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProfileSearchVH -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<ProfileEntity>) {
        differ.submitList(list)
    }

    class ProfileSearchVH
    constructor(
        itemView: View,
        private val interaction: Interaction?,
        private val prefs: SharedPreferencesStorage
    ) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: ProfileEntity) = with(itemView) {
            val mResult = itemView.findViewById<AppCompatTextView>(R.id.tv_result)
            val mUpdate = itemView.findViewById<MaterialButton>(R.id.btn_update)
            if (item.isUpdated) {
                mUpdate.text = "Updated"
                mUpdate.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen))
            } else {
                mUpdate.text = "Update"
                mUpdate.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBlue))
            }
            mResult.text =
                Html.fromHtml("<b>Name:</b> ${item.name} <br/> <b>Father name:</b> ${item.fatherName ?: "N/A"} <br/> <b>Mother name:</b> ${item.motherName} <br/> <b>Mobile Number:</b> ${item.mobileNo} <br/>")
            mUpdate.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: ProfileEntity)
    }
}