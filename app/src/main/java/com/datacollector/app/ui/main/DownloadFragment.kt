package com.datacollector.app.ui.main

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.datacollector.app.R
import com.datacollector.app.databinding.FragmentDownloadBinding
import com.datacollector.app.local.AppDao
import com.datacollector.app.local.ContractEntity
import com.datacollector.app.local.PartnerEntity
import com.datacollector.app.local.ProfileEntity
import com.datacollector.app.ui.ContractsSpinnerAdapter
import com.datacollector.app.ui.PartnerSpinnerAdapter
import com.datacollector.app.ui.auth.CONNECTION_ERROR
import com.datacollector.app.ui.auth.SUCCESS
import com.datacollector.app.utils.InternetCheck
import com.datacollector.app.utils.RemoteDatabaseConnection
import com.datacollector.app.utils.SharedPreferencesStorage
import com.google.android.material.snackbar.Snackbar
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import javax.inject.Inject

class DownloadFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val connection: Connection?,
    private val dao: AppDao
) : Fragment(), AdapterView.OnItemSelectedListener {

    private var partnerSelected: String = ""
    private var allPartners: ArrayList<PartnerEntity> = ArrayList()
    private var resultProfiles: ArrayList<ProfileEntity> = ArrayList()
    private var partnerId: Int = 0
    private var contractSelected: String = ""
    private var allContracts: ArrayList<ContractEntity> = ArrayList()

    private var selectedContractId = 0

    var newConnection: Connection? = null

    lateinit var binding: FragmentDownloadBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDownloadBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newConnection = RemoteDatabaseConnection().execute().get()
        binding.spinnerPartner.onItemSelectedListener = this
        binding.spinnerContract.onItemSelectedListener = this
        binding.btnDownload.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            initDownloadQuery()
        }
        observeLocalPartners()
    }

    private fun observeLocalContracts() {
        dao.getLocalContractList().removeObservers(viewLifecycleOwner)
        dao.getLocalContractList().observe(viewLifecycleOwner, Observer {
            allContracts.clear()
            allContracts.addAll(it)
            binding.spinnerContract.apply {
                adapter =
                    ContractsSpinnerAdapter(
                        requireContext(),
                        it
                    )
            }
        })
    }

    private fun observeLocalPartners() {
        dao.getLocalPartnerList().removeObservers(viewLifecycleOwner)
        dao.getLocalPartnerList().observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                newConnection = RemoteDatabaseConnection().execute().get()
                initPartnerQuery()
            }
            allPartners.clear()
            allPartners.addAll(it)
            binding.spinnerPartner.apply {
                adapter = PartnerSpinnerAdapter(
                    requireContext(),
                    it
                )
            }
        })
    }

    private fun initDownloadQuery() {
        newConnection = RemoteDatabaseConnection().execute().get()
        val downloadAsync = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String {
                if (connection != null) {
                    val statement: Statement?
                    return try {
                        statement =
                            newConnection!!.prepareCall("{call procGetProfileList @ContractID=$selectedContractId}")
                        statement.execute()
                        val resultSet: ResultSet = statement.resultSet
                        while (resultSet.next()) {
                            resultProfiles.add(
                                ProfileEntity(
                                    resultSet.getInt("ProfileID"),
                                    resultSet.getString("Name"),
                                    resultSet.getString("FamilyHeadName"),
                                    resultSet.getString("MobileNo"),
                                    resultSet.getString("FatherName"),
                                    resultSet.getString("MotherName"),
                                    resultSet.getString("District"),
                                    resultSet.getString("Municipality"),
                                    resultSet.getString("WardNo"),
                                    resultSet.getString("Tole"),
                                    resultSet.getString("Commodity"),
                                    resultSet.getString("LandUnit"),
                                    false
                                )
                            )
                        }
                        SUCCESS
                    } catch (e: Exception) {
                        "Failed"
                    }
                } else {
                    return CONNECTION_ERROR
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                if (result == CONNECTION_ERROR) {
                    this.execute()
                }
                binding.progressBar.visibility = View.GONE
                if (resultProfiles.isNotEmpty()) {
                    resultProfiles.forEach {
                        dao.addProfile(it)
                    }
                    val localProfileList = dao.getLocalProfileList()
                    Snackbar.make(
                        requireView(),
                        "${localProfileList.count()} profiles saved in phone memory.",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
        downloadAsync.execute()
    }

    private fun initPartnerQuery() {
        val allPartnersAsync = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String {
                if (newConnection != null) {
                    val statement: Statement?
                    return try {
                        if (prefs.partnerID != "0") {
                            statement = newConnection!!.prepareCall("{call procGetPartnerList(?)}")
                            statement.setString(1, prefs.partnerID)
                        } else {
                            statement = newConnection!!.prepareCall("{call procGetPartnerList}")
                        }
                        statement.execute()
                        val resultSet: ResultSet = statement.resultSet
                        while (resultSet.next()) {
                            allPartners.add(
                                PartnerEntity(
                                    resultSet.getInt("PartnerID"),
                                    resultSet.getString("PartnerName")
                                )
                            )
                        }
                        statement.close()
                        SUCCESS
                    } catch (e: SQLException) {
                        e.printStackTrace()
                        "Failed"
                    }
                } else {
                    return CONNECTION_ERROR
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                if (result == SUCCESS) {
                    binding.progressBar.visibility = View.GONE
                    Log.i("Intro", result.toString())
                    allPartners.forEach {
                        dao.addPartner(it)
                    }
                } else {
                    binding.progressBar.visibility = View.GONE
                    Log.e("Intro", result.toString())
                }
            }
        }
        allPartnersAsync.execute()
    }

    private fun initContractQuery(queryPartnerId: Int) {
        val allContractsAsync = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String {
                if (newConnection != null) {
                    val statement: Statement?
                    return try {
                        statement =
                            newConnection!!.prepareCall("{call procContractList @PartnerID=$queryPartnerId}")
                        statement.execute()
                        allContracts.clear()
                        val resultSet: ResultSet = statement.resultSet
                        while (resultSet.next()) {
                            allContracts.add(
                                ContractEntity(
                                    resultSet.getInt("ContractID"),
                                    resultSet.getString("ContractTitle")
                                )
                            )
                        }
                        statement.close()
                        SUCCESS
                    } catch (e: SQLException) {
                        e.printStackTrace()
                        "Failed"
                    }
                } else {
                    return CONNECTION_ERROR
                }
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                if (result == SUCCESS) {
                    Log.i("Intro", result.toString())
                    dao.clearContractData()
                    allContracts.forEach {
                        dao.addContract(it)
                    }
                    observeLocalContracts()
                } else {
                    observeLocalContracts()
                    Log.e("Intro", result.toString())
                }
            }
        }
        allContractsAsync.execute()
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        when (parent) {
            binding.spinnerPartner -> {
                partnerSelected = allPartners[position].partnerName
                partnerId = allPartners[position].partnerId
                initContractQuery(partnerId)
            }
            binding.spinnerContract -> {
                contractSelected = allContracts[position].contractTitle
                selectedContractId = allContracts[position].contractId
            }
        }
    }

}