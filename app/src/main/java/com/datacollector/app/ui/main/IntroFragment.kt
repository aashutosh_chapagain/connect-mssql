package com.datacollector.app.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.datacollector.app.R
import com.datacollector.app.local.AppDao
import com.datacollector.app.local.ProfileEntity
import com.datacollector.app.utils.SharedPreferencesStorage
import kotlinx.android.synthetic.main.fragment_intro.*
import java.sql.Connection
import javax.inject.Inject


class IntroFragment
@Inject
constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : Fragment(), ProfileSearchAdapter.Interaction {

    lateinit var mSearchView: SearchView
    private val searchResults = arrayListOf<ProfileEntity>()

    private val profileSearchAdapter = ProfileSearchAdapter(this, prefs)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_intro, container, false)
        mSearchView = view.findViewById(R.id.search_view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //handleBackKey(view)
        rv_search_results.apply {
            adapter = profileSearchAdapter
            isNestedScrollingEnabled = false
        }
        showAllDocuments()
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    dao.searchLocalProfileByName(query)
                    initSearch(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.isNotEmpty()) {
                    dao.searchLocalProfileByName(newText)
                    initSearch(newText)
                }
                return true
            }
        })
        mSearchView.setOnCloseListener {
            showAllDocuments()
            false
        }
    }

    private fun showAllDocuments() {
        dao.getProfileList().removeObservers(viewLifecycleOwner)
        dao.getProfileList().observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                tv_no_data.visibility = View.VISIBLE
            } else {
                tv_no_data.visibility = View.GONE
                profileSearchAdapter.submitList(it)
                profileSearchAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun initSearch(search: String) {
        dao.searchLocalProfileByName(search).removeObservers(viewLifecycleOwner)
        dao.searchLocalProfileByName(search).observe(viewLifecycleOwner, Observer { profileList ->
            searchResults.clear()
            searchResults.addAll(profileList)
            profileSearchAdapter.submitList(searchResults)
            profileSearchAdapter.notifyDataSetChanged()
        })
    }

    override fun onItemSelected(position: Int, item: ProfileEntity) {
        item
        (activity as MainActivity).navController()
            .navigate(BottomNavFragmentDirections.actionBottomNavFragmentToFormFragment(item))
    }

//    private fun handleBackKey(view: View) {
//        val callback = object : OnBackPressedCallback(true) {
//            override fun handleOnBackPressed() {
//                val alertDialog = MaterialAlertDialogBuilder(requireContext())
//                alertDialog.setTitle("Log out")
//                alertDialog.setMessage("Are you sure to log out?")
//                alertDialog.setPositiveButton("Yes") { dialog, _ ->
//                    // do logout
//                    prefs.isLogin = false
//                    startActivity(Intent(requireContext(), SplashActivity::class.java))
//                    requireActivity().finish()
//                    dialog.dismiss()
//                }
//                alertDialog.setNegativeButton("No") { dialog, _ ->
//                    dialog.dismiss()
//                }
//                alertDialog.show()
//            }
//        }
//        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
//    }

}