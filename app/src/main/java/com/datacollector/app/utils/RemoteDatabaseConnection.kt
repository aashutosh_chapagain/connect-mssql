package com.datacollector.app.utils

import android.os.AsyncTask
import android.os.StrictMode
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import javax.inject.Singleton

@Singleton
class RemoteDatabaseConnection: AsyncTask<Void, Void, Connection?>() {

    private val ip = "110.34.31.85"
    private val port = "1433"
    private val Classes = "net.sourceforge.jtds.jdbc.Driver"
    private val database = "testing"
    private val username = "test"
    private val password = "123"
    private val url = "jdbc:jtds:sqlserver://$ip:$port/$database"

    private var connection: Connection? = null

    override fun doInBackground(vararg p0: Void?): Connection? {
        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        return try {
            Class.forName(Classes)
            connection = DriverManager.getConnection(url, username, password)
            connection
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
            connection
        } catch (e: SQLException) {
            e.printStackTrace()
            connection
        }
    }
}