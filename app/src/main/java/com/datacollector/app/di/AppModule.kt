package com.datacollector.app.di

import android.app.Application
import com.datacollector.app.local.AppDao
import com.datacollector.app.local.AppDatabase
import com.datacollector.app.utils.RemoteDatabaseConnection
import com.datacollector.app.utils.SharedPreferencesStorage
import dagger.Module
import dagger.Provides
import java.sql.Connection
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object AppModule {

    @Singleton
    @Provides
    @JvmStatic
    fun provideStoragePrefs(context: Application): SharedPreferencesStorage {
        return SharedPreferencesStorage(
            context
        )
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideDatabase(context: Application): AppDatabase {
        return AppDatabase.invoke(context)
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideDao(appDatabase: AppDatabase): AppDao {
        return appDatabase.appDao()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideRemoteDatabaseConnection(): Connection? {
        return RemoteDatabaseConnection().execute().get()
    }
}