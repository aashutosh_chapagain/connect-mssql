package com.datacollector.app

import android.app.Application
import androidx.multidex.MultiDex
import com.datacollector.app.di.AppComponent
import com.datacollector.app.di.DaggerAppComponent
import com.datacollector.app.di.auth.AuthComponent
import com.datacollector.app.di.main.MainComponent
import com.datacollector.app.di.splash.SplashComponent

class BaseApplication : Application() {
    private lateinit var appComponent: AppComponent
    private var mainComponent: MainComponent? = null
    private var authComponent: AuthComponent? = null
    private var splashComponent: SplashComponent? = null

    override fun onCreate() {
        super.onCreate()
        //MultiDex
        MultiDex.install(this@BaseApplication)
        initAppComponent()

    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    fun mainComponent(): MainComponent {
        if (mainComponent == null) {
            mainComponent = appComponent.mainComponent().create()
        }
        return mainComponent as MainComponent
    }

    fun authComponent(): AuthComponent {
        if (authComponent == null) {
            authComponent = appComponent.authComponent().create()
        }
        return authComponent as AuthComponent
    }

    fun splashComponent(): SplashComponent {
        if (splashComponent == null) {
            splashComponent = appComponent.splashComponent().create()
        }
        return splashComponent as SplashComponent
    }

    fun releaseMainComponent() {
        authComponent = null
    }

    fun releaseSplashComponent() {
        splashComponent = null
    }

    fun releaseAuthComponent() {
        authComponent = null
    }
}