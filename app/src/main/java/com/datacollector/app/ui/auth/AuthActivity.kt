package com.datacollector.app.ui.auth

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentFactory
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.datacollector.app.BaseActivity
import com.datacollector.app.BaseApplication
import com.datacollector.app.R
import com.datacollector.app.fragments.auth.AuthNavHostFragment
import com.datacollector.app.utils.SharedPreferencesStorage
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject


const val SUCCESS = "Success"
const val INVALID_CREDENTIALS = "Invalid credentials"
const val FAILED = "Login failed. Please try again."
const val CONNECTION_ERROR = "Connection Failed"
const val VALIDATION_ERROR = "Please input all fields"

class AuthActivity : BaseActivity() {

    lateinit var mFirebaseAnalytics: FirebaseAnalytics

    override fun inject() {
        (application as BaseApplication).authComponent().inject(this)
    }
    @Inject
    lateinit var fragmentFactory: FragmentFactory
    @Inject
    lateinit var prefs: SharedPreferencesStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        onRestoreInstanceState()
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    private fun onRestoreInstanceState() {
        val host = supportFragmentManager
            .findFragmentById(R.id.auth_fragment_container)
        host?.let {
            Log.i("Main", "we are here")
        } ?: createNavHostFragment()
    }

    private fun createNavHostFragment() {
        val navHost = AuthNavHostFragment.create(R.navigation.auth_navigation)
        supportFragmentManager.beginTransaction()
            .replace(R.id.auth_fragment_container, navHost, "AuthNavHost")
            .setPrimaryNavigationFragment(navHost)
            .commit()
    }

    fun navController(): NavController {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.auth_fragment_container) as NavHostFragment
        return navHostFragment.navController
    }

    override fun onDestroy() {
        (application as BaseApplication).releaseAuthComponent()
        super.onDestroy()
    }

}