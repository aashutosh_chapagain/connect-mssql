package com.datacollector.app.fragments.auth

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.datacollector.app.ui.auth.LoginFragment
import com.datacollector.app.utils.SharedPreferencesStorage
import com.datacollector.app.di.main.MainScope
import com.datacollector.app.local.AppDao
import java.sql.Connection
import javax.inject.Inject

@MainScope
class AuthFragmentFactory
@Inject constructor(
    private val prefs: SharedPreferencesStorage,
    private val dao: AppDao
) : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when (className) {
            LoginFragment::class.java.name -> {
                LoginFragment(prefs, dao)
            }
            else -> {
                LoginFragment(prefs, dao)
            }
        }
}