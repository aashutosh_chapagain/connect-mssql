package com.datacollector.app.di

import android.app.Application
import com.datacollector.app.BaseActivity
import com.datacollector.app.di.auth.AuthComponent
import com.datacollector.app.di.main.MainComponent
import com.datacollector.app.di.splash.SplashComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, SubComponentModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(baseActivity: BaseActivity)

    fun mainComponent(): MainComponent.Factory
    fun splashComponent(): SplashComponent.Factory
    fun authComponent(): AuthComponent.Factory


}