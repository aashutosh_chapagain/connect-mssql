package com.datacollector.app.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import com.datacollector.app.BaseActivity
import com.datacollector.app.BaseApplication
import com.datacollector.app.local.AppDao
import com.datacollector.app.ui.auth.AuthActivity
import com.datacollector.app.ui.main.MainActivity
import com.datacollector.app.utils.SharedPreferencesStorage
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    override fun inject() {
        (application as BaseApplication).splashComponent().inject(this)
    }

    @Inject
    lateinit var prefs: SharedPreferencesStorage

    @Inject
    lateinit var dao: AppDao


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Full Screen
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }
        if (prefs.isLogin) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, AuthActivity::class.java))
        }
        finish()
    }



    override fun onDestroy() {
        (application as BaseApplication).releaseSplashComponent()
        super.onDestroy()
    }

}