package com.datacollector.app.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

const val FULL_NAME = "FULL_NAME"
const val USER_NAME = "USER_NAME"
const val PARTNER_NAME = "PARTNER_NAME"
const val USER_ID = "USER_ID"
const val PARTNER_ID = "PATTERN_ID"
const val IS_LOGIN = "IS_LOGIN"

class SharedPreferencesStorage(context: Application) {

    private val prefs: SharedPreferences = context.getSharedPreferences(
        "com.datacollector.app.prefs",
        Context.MODE_PRIVATE
    )

    var fullName: String
        get() = prefs.getString(FULL_NAME, "") ?: ""
        set(value) = prefs.edit().putString(FULL_NAME, value).apply()

    var userName: String
        get() = prefs.getString(USER_NAME, "") ?: ""
        set(value) = prefs.edit().putString(USER_NAME, value).apply()

    var partnerName: String
        get() = prefs.getString(PARTNER_NAME, "") ?: ""
        set(value) = prefs.edit().putString(PARTNER_NAME, value).apply()

    var userID: String
        get() = prefs.getString(USER_ID, "") ?: ""
        set(value) = prefs.edit().putString(USER_ID, value).apply()

    var partnerID: String
        get() = prefs.getString(PARTNER_ID, "") ?: ""
        set(value) = prefs.edit().putString(PARTNER_ID, value).apply()

    var isLogin: Boolean
        get() = prefs.getBoolean(IS_LOGIN, false)
        set(value) = prefs.edit().putBoolean(IS_LOGIN, value).apply()

    fun clearPrefs() {
        prefs.edit().clear().apply()
    }
}